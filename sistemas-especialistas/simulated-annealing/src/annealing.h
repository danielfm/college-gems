/*
   Name: Solving the Travelling Salesman problem with Simulated Annealing.
   Authors: Daniel F. Martins
   Date: 07/05/2006
*/

#ifndef MAIN_H_
#define MAIN_H_


/* --------------------------------- */
/* CONSTANTES                        */
/* --------------------------------- */

#define TRUE 1
#define FALSE 0

#define LOG_FILE_NAME "trace.log"

/**
 * Total de elementos definidos na matriz.
 */
#define ELEMENTS 5


/* --------------------------------- */
/* MÉTODOS                           */
/* --------------------------------- */

/**
 * Obtém um novo seed para geração de números aleatórios,
 * com base na data do sistema.
 */
void seed();

/**
 * Seleciona um elemento randômico (0 .. ELEMENTS - 1)
 */
int pick_random_element();

/**
 * Configura as variáveis utilizadas no cálculo para seus valores
 * iniciais.
 */
void reset();

/**
 * Verifica se um nó já faz parte da solução.
 */
int in_solution(int node, int total);

/**
 * Gera uma solução randômica.
 */
void generate_random_solution();

/**
 * Imprime a solução obtida.
 */
void print_solution();

/**
 * Calcula o custo da solução obtida.
 */
int cost();

/**
 * Imprime o cabeçalho do programa.
 */
void print_header();

/**
 * Cria o arquivo de log.
 */
void create_log_file();

/**
 * Inclui uma mensagem no log.
 */
void log_message(char *message);

/**
 * Fecha o arquivo de log.
 */
void close_log_file();

/**
 * Imprime a matriz de testes.
 */
void print_matrix();

/**
 * Troca dois valores de posição na solução.
 */
void swap(int e1, int e2);

/**
 * Calcula uma solução segundo o algoritmo Simulated Annealing.
 */
void calculate();

/**
 * Algoritmo de Boltzmann, que determina se uma 'má troca' deve
 * ser concretizada ou desfeita.
 */
int boltzmann(float delta);


/* --------------------------------- */
/* VARIÁVEIS                         */
/* --------------------------------- */

/**
 * Matriz utilizada no exemplo.
 */
const int matrix[ELEMENTS][ELEMENTS] = {
  {0, 1, 2, 7, 5},
  {1, 0, 3, 4, 3},
  {2, 3, 0, 5, 2},
  {7, 4, 5, 0, 3},
  {5, 3, 2, 3, 0}
};

/**
 * Vetor que armazena a solução.
 */
int solution[ELEMENTS];

/**
 * Número de trocas bem sucedidas.
 */
int good_swaps;

/**
 * Número máximo de trocas bem sucedidas. Se este número for
 * alcançado antes do número máximo de iterações por temperatura,
 * o algoritmo passa para a próxima temperatura.
 */
int gs_limit;

/**
 * Número máximo de iterações feitas antes que a temperatura
 * seja diminuída.
 */
int n_limit;

/**
 * Temperatura atual.
 */
double temperature;

/**
 * Temperatura mínima. Quando a temperatura alcançar este valor
 * o algoritmo terminará.
 */
double min_temperature;

/**
 * Valor que será utilizado para fazer o decremento da temperatura.
 */
double t_delta;

/**
 * Arquivo de log.
 */
FILE *log_file;

#endif /*MAIN_H_*/
