/*
   Name: Solving the Travelling Salesman problem with Simulated Annealing.
   Authors: Daniel F. Martins
   Date: 07/05/2006
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

#include "annealing.h"

int main() {
  create_log_file();

  log_message("***************************");
  log_message("** Iniciando nova sessao **");
  log_message("***************************\n");

  seed();

  print_header();
  print_matrix();

  reset();

  printf("\n* Gerando uma solucao aleatoria...\n\n");
  generate_random_solution();

  print_solution();

  printf("\n* Calculando nova solucao...\n\n");
  calculate();

  print_solution();

  log_message("***************************");
  log_message("**   Encerrando sessao   **");
  log_message("***************************\n");

  close_log_file();

  return 0;
}

void print_header() {
  printf("\n\n");
  printf("Sistemas Especialistas - Simulated Annealing\n");
  printf("********************************************\n\n");

  printf("PROBLEMA DO CAIXEIRO VIAJANTE\n\n");
}

void create_log_file() {
  log_file = fopen(LOG_FILE_NAME, "a+");

  if (log_file == NULL) {
    exit(1); /* Erro ao criar o arquivo de log */
  }

  log_message("Abrindo o arquivo de log.");
}

void log_message(char *message) {
  fprintf(log_file, "* %s\n", message);
}

void close_log_file() {
  if (log_file != NULL) {
    log_message("Fechando o arquivo de log.");
    fclose(log_file);
  }
}

void print_matrix() {
  int i,j;

  for (i = 0; i < ELEMENTS; i++) {
    for (j = 0; j < ELEMENTS; j++) {
      printf("%d\t", matrix[i][j]);
    }
    printf("\n");
  }
}

void seed() {
  srand((unsigned)time(NULL));
}

int pick_random_element() {
  return rand() % ELEMENTS;
}

void reset() {
  int i;

  log_message("Configurando as variaveis com seus valores iniciais.");

  temperature = 20;
  min_temperature = 0.01;
  t_delta = 0.8;

  n_limit = 20;

  good_swaps = 0;
  gs_limit = 10;

  for (i = 0; i < ELEMENTS; i++) {
    solution[i] = -1;
  }
}

int in_solution(int node, int total) {
  int i;

  for (i = 0; i < total; i++) {
    if (node == solution[i]) {
      return TRUE;
    }
  }
  return FALSE;
}

void generate_random_solution() {
  int current_node = pick_random_element();
  int found;
  int element;

  int total = 0;
  solution[total++] = current_node;

  log_message("Gerando uma solucao aleatoria...");

  while (total < ELEMENTS) {
    element = 0;

    while (TRUE) {
      element = pick_random_element();
      found = in_solution(element, total);

      if (matrix[current_node][element] > 0 && !found) {
        solution[total++] = element;
        current_node = element;
        break;
      }
    }
  }

  log_message("Solucao aleatoria gerada.");
}

void swap(int e1, int e2) {
  int aux = solution[e1];
  solution[e1] = solution[e2];
  solution[e2] = aux;
}

void calculate() {
  int i;
  int e1, e2;
  int old_cost, new_cost, cost_delta;

  char *message = (char*)malloc(sizeof(char) * 50);
  int it = 0; /* Número de iterações */

  log_message("Calculando a solucao...");

  while (temperature > min_temperature) {
    good_swaps = 0;

    for (i = 0; i < n_limit; i++, ++it) {
      old_cost = cost();

      /* Seleciona dois elementos aleatoriamente */
      e1 = pick_random_element();

      do {
        e2 = pick_random_element();
      } while (e2 == e1);

      /* Troca os elementos e calcula o novo custo */
      swap(e1, e2);
      new_cost = cost();

      cost_delta = new_cost - old_cost;

      if (cost_delta > 0) {
        sprintf(message, "O custo da solucao aumentou de %d para %d.", old_cost, new_cost);
        log_message(message);
        log_message("Verificando se a troca sera efetivada...");

        if (!boltzmann(cost_delta)) {
          log_message("Solucao anterior restaurada.");

          /* Desfaz a troca */
          swap(e1, e2);
          --good_swaps;
        }
      }

      /**
       * Parte para a próxima temperatura caso já tenha feito
       * muitas trocas boas
       */
      if (++good_swaps > gs_limit) {
        break;
      }
    }

    sprintf(message, "Nova temperatura %+f.", temperature);
    log_message(message);

    temperature *= t_delta;
  }

  log_message("Solucao calculada.");

  sprintf(message, "Numero de iteracoes realizadas: %d.", it);
  log_message(message);
}

void print_solution() {
  char *message = (char*)malloc(sizeof(char) * 50);

  int i;
  for (i = 0; i < ELEMENTS; i++) {
    printf("%s%d", (i > 0 ? " - " : ""), (solution[i] + 1));
  }

  printf(". Custo = %d\n", cost());

  sprintf(message, "Custo da solucao: %d.", cost());
  log_message(message);
}

int cost() {
  int i, cost = 0;
  int previous = solution[0];

  for (i = 1; i < ELEMENTS; previous = solution[i++]) {
    cost += matrix[previous][solution[i]];
  }

  cost += matrix[previous][solution[0]];
  return cost;
}

int boltzmann(float delta) {
  float random = (float)(rand() % 100) / 100.0;
  float metropolis = exp((-delta) / temperature);

  if (random < metropolis) {
    log_message("Solucao atual aceita.");
    return TRUE;
  }

  log_message("Solucao atual NAO foi aceita.");
  return FALSE;
}
